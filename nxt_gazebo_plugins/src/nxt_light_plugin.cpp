/*
 * Copyright (c) 2012, Miguel Gómez Gonzalo.
 * Copyright (c) 2012, Ana M. Cruz Martín.
 * Copyright (c) 2012, Juan A. Fernández Madrigal.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <nxt_gazebo_plugins/nxt_light_plugin.h>

//copied from gazebo_ros_template
#include <gazebo/Global.hh>
#include <gazebo/XMLConfig.hh>
#include <gazebo/gazebo.h>
#include <gazebo/GazeboError.hh>
#include <gazebo/ControllerFactory.hh>

//added by myself
#include <gazebo/Simulator.hh>
#include <gazebo/Sensor.hh>


using namespace gazebo;

GZ_REGISTER_DYNAMIC_CONTROLLER("nxt_light_plugin", NxtLightPlugin);

////////////////////////////////////////////////////////////////////////////////
// Constructor
NxtLightPlugin::NxtLightPlugin(Entity *parent) : Controller(parent)
{
    parent_ = dynamic_cast<Model*> (parent);
    
    if(!parent_)
      gzthrow("Nxt_Light_Plugin controller requires a Model as its parent");
    
    Param::Begin(&parameters);
    pubTopicNameP_ = new ParamT<std::string>("pubTopicName", "", false);
    subTopicNameP_ = new ParamT<std::string> ("subTopicName", "", false);
    robotNamespaceP_ = new ParamT<std::string> ("robotNamespace", "", false);
    Param::End();

}

////////////////////////////////////////////////////////////////////////////////
// Destructor
NxtLightPlugin::~NxtLightPlugin()
{

    delete pubTopicNameP_;
    delete subTopicNameP_;
    delete robotNamespaceP_;
    
}

////////////////////////////////////////////////////////////////////////////////
// Load the controller
void NxtLightPlugin::LoadChild(XMLConfigNode *node)
{
    ROS_INFO("INFO: nxt_light_plugin loading");

    robotNamespaceP_->Load(node);
    robotNamespace_ = robotNamespaceP_->GetValue();

    
    pubTopicNameP_->Load(node);
    pubTopicName_ = pubTopicNameP_->GetValue();

    subTopicNameP_->Load(node);
    subTopicName_ = subTopicNameP_->GetValue();

}

////////////////////////////////////////////////////////////////////////////////
// Initialize the controller
void NxtLightPlugin::InitChild()
{
	int argc = 0;
	char** argv = NULL;
	ros::init(argc, argv, "nxt_light_plugin", ros::init_options::NoSigintHandler | ros::init_options::AnonymousName);
	rosnode_ = new ros::NodeHandle(robotNamespace_);
    
	// ROS: Subscribe to the camera image topic (usually "light_sensor_camera")
	sub_ = rosnode_->subscribe(subTopicName_, 1, &NxtLightPlugin::onCameraMsg, this);
	pub_ = rosnode_->advertise<std_msgs::Int16>(pubTopicName_, 1);

    lightValue_ = 0;
}

////////////////////////////////////////////////////////////////////////////////
// Update the controller
void NxtLightPlugin::UpdateChild()
{
    /***************************************************************/
    /*                                                             */
    /*  this is called at every update simulation step             */
    /*                                                             */
    /***************************************************************/
    

    std_msgs::Int16 msg;
    
    msg.data = lightValue_;
    pub_.publish(msg);
    ros::spinOnce();

}

////////////////////////////////////////////////////////////////////////////////
// Finalize the controller
void NxtLightPlugin::FiniChild()
{
  rosnode_->shutdown();
  delete rosnode_;
}

////////////////////////////////////////////////////////////////////////////////
// Procedure for the subscriber
void NxtLightPlugin::onCameraMsg( const sensor_msgs::ImageConstPtr &msg)
{


    int r8, g8, b8; // R8G8B8 image values
    int imgWidth, imgHeight; // image size

    imgWidth = msg->width;
    imgHeight = msg->height;
    int actualImage[imgHeight*imgWidth*3]; // actual image

    for(int i=0;i<imgHeight*imgWidth*3;i++){
        actualImage[i] = msg->data[i];
    }

    r8 = 0;
    g8 = 0;
    b8 = 0;

    for(int i=0;i<(imgHeight*imgWidth*3);i=i+imgWidth*3){
        for(int j=0;j<imgWidth*3;j=j+3){
            r8 += actualImage[i+j];
            g8 += actualImage[i+j+1];
            b8 += actualImage[i+j+2];
        }
    }

    // light value [0..255]
    lightValue_ = (int)((0.299*r8 + 0.587*g8 + 0.114*b8) / (imgHeight*imgWidth));

}
