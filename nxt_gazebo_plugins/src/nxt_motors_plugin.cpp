/*
 * Copyright (c) 2012, Miguel Gómez Gonzalo.
 * Copyright (c) 2012, Ana M. Cruz Martín.
 * Copyright (c) 2012, Juan A. Fernández Madrigal.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <algorithm>
#include <assert.h>

#include <nxt_gazebo_plugins/nxt_motors_plugin.h>

//copied from gazebo_ros_template
#include <gazebo/Global.hh>
#include <gazebo/XMLConfig.hh>
#include <gazebo/gazebo.h>
#include <gazebo/GazeboError.hh>
#include <gazebo/ControllerFactory.hh>

//added by myself
#include <gazebo/Simulator.hh>
#include <gazebo/Entity.hh>
#include <gazebo/Quatern.hh>
#include <gazebo/Controller.hh>
#include <gazebo/Body.hh>
#include <gazebo/World.hh>
#include <gazebo/PhysicsEngine.hh>

using namespace gazebo;

GZ_REGISTER_DYNAMIC_CONTROLLER("nxt_motors_plugin", NxtMotorsPlugin);

enum
{
	RIGHT, LEFT
};

////////////////////////////////////////////////////////////////////////////////
// Constructor
NxtMotorsPlugin::NxtMotorsPlugin(Entity *parent)
    : Controller(parent)
{
    parent_ = dynamic_cast<Model*> (parent);
    
    if(!parent_)
      gzthrow("Nxt_Motors_Plugin controller requires a Model as its parent");
    
    Param::Begin(&parameters);
    leftJointNameP_ = new ParamT<std::string> ("leftJoint", "", 1);
    rightJointNameP_ = new ParamT<std::string> ("rightJoint", "", 1);
    torqueP_ = new ParamT<float> ("torque", 0.15, 1); //TODO: Change default if needed
    robotNamespaceP_ = new ParamT<std::string> ("robotNamespace", "/", 0);
    pubTopicNameP_ = new ParamT<std::string> ("pubTopicName", "", 1);
    subTopicNameP_ = new ParamT<std::string> ("subTopicName", "", 1);
    Param::End();

}

////////////////////////////////////////////////////////////////////////////////
// Destructor
NxtMotorsPlugin::~NxtMotorsPlugin()
{
    delete leftJointNameP_;
    delete rightJointNameP_;
    delete torqueP_;
    delete robotNamespaceP_;
    delete pubTopicNameP_;
    delete subTopicNameP_;
    
}

////////////////////////////////////////////////////////////////////////////////
// Load the controller
void NxtMotorsPlugin::LoadChild(XMLConfigNode *node)
{
    ROS_INFO("INFO: nxt_motors_plugin loading");

   
    torqueP_->Load(node);
    torque_ = torqueP_->GetValue();

    leftJointNameP_->Load(node);
    leftJointName_ = leftJointNameP_->GetValue();
    rightJointNameP_->Load(node);
    rightJointName_ = rightJointNameP_->GetValue();
    
    joints_[LEFT] = parent_->GetJoint(**leftJointNameP_);
    joints_[RIGHT] = parent_->GetJoint(**rightJointNameP_);
    
    if(!joints_[LEFT])
		gzthrow("The controller couldn't get left hinge joint");
	
    if(!joints_[RIGHT])
		gzthrow("The controller couldn't get right hinge joint");
		
	robotNamespaceP_->Load(node);
    robotNamespace_ = robotNamespaceP_->GetValue();
	
    subTopicNameP_->Load(node);
    subTopicName_ = subTopicNameP_->GetValue();

    pubTopicNameP_->Load(node);
    pubTopicName_ = pubTopicNameP_->GetValue();
    
}

////////////////////////////////////////////////////////////////////////////////
// Initialize the controller
void NxtMotorsPlugin::InitChild()
{
    enableMotors_ = true;
    
    wheelSpeed_[RIGHT] = 0;
    wheelSpeed_[LEFT] = 0;
    
    prevUpdateTime_ = Simulator::Instance()->GetSimTime();

	odomPose_[0] = 0.0;
	odomPose_[1] = 0.0;
	
	odomVel_[0] = 0.0;
	odomVel_[1] = 0.0;

    int argc = 0;
    char** argv = NULL;
    ros::init(argc, argv, "nxt_motors_plugin", ros::init_options::NoSigintHandler | ros::init_options::AnonymousName);
    rosnode_ = new ros::NodeHandle(robotNamespace_);
    
    // ROS: Subscribe to the velocity command topic (usually "cmd_vel")
    sub_ = rosnode_->subscribe(subTopicName_, 1, &NxtMotorsPlugin::onCmdVel, this);
    pub_ = rosnode_->advertise<nxt_gazebo_plugins::nxtOdom>(pubTopicName_, 1);
}

////////////////////////////////////////////////////////////////////////////////
// Update the controller
void NxtMotorsPlugin::UpdateChild()
{
    /***************************************************************/
    /*                                                             */
    /*  this is called at every update simulation step             */
    /*                                                             */
    /***************************************************************/
    
    Time stepTime, auxTime;
    
    auxTime = Simulator::Instance()->GetSimTime();
    stepTime = auxTime - prevUpdateTime_;
    prevUpdateTime_ = auxTime;

    odomVel_[0] = joints_[LEFT]->GetVelocity(0);
    odomVel_[1] = joints_[RIGHT]->GetVelocity(0);

    odomPose_[0] += odomVel_[0] * stepTime;
    odomPose_[1] += odomVel_[1] * stepTime;

    Odom_.speed.left = odomVel_[0]; //Odom_.angular is turning speed of motors
    Odom_.speed.right = odomVel_[1];

    Odom_.position.left = odomPose_[0]; // Odom_.linear is position of wheels
    Odom_.position.right = odomPose_[1]; // in radians

    pub_.publish(Odom_);
    ros::spinOnce();

    if(enableMotors_)
    {
		joints_[LEFT]->SetVelocity(0, wheelSpeed_[LEFT]);
		joints_[RIGHT]->SetVelocity(0, wheelSpeed_[RIGHT]);
		
		joints_[LEFT]->SetMaxForce(0, **(torqueP_));
		joints_[RIGHT]->SetMaxForce(0, **(torqueP_));
	}
}

////////////////////////////////////////////////////////////////////////////////
// Procedure for the subscriber
void NxtMotorsPlugin::onCmdVel( const nxt_gazebo_plugins::nxtMotorsConstPtr &msg)
{
    wheelSpeed_[LEFT] = msg->left;
    wheelSpeed_[RIGHT] = msg->right;
}