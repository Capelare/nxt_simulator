/*
 * Copyright (c) 2012, Miguel Gómez Gonzalo.
 * Copyright (c) 2012, Ana M. Cruz Martín.
 * Copyright (c) 2012, Juan A. Fernández Madrigal.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <errno.h>
//#include <fstream.h>

using namespace std;

int main(int argc, char *argv[]){
  if(argc != 2){
    cout << "Usage: rosrun nxt_gazebo_plugins nxc_parser file.nxc" << endl;
    return -1;
  }

  // The path is stored in argv[0]
  char path[256];
  strcpy(path, argv[0]);

  // Removing the name of the program
  char *last_slash = strrchr(path, '/');
  *last_slash = '\0';

  // Going up one level
  last_slash = strrchr(path, '/');
  *last_slash = '\0';

  // Getting the final path
  char basefname[256] = "";
  strcat(basefname, path);
  strcat(basefname, "/src/nxt_base_file.cpp");

  // Creating the output file in the same folder
  char outputfname[256] = "";
  strcat(outputfname, path);
  strcat(outputfname, "/src/nxc_gazebo.cpp");
  FILE *outputFile = fopen(outputfname, "w");
  if(!outputFile){
    perror("Could not create nxc_gazebo.cpp");
    return 1;
  }

  FILE *baseFile = fopen(basefname, "r");
  if(!baseFile){
    fprintf(stderr, "Could not open: nxt_base_file.cpp %s\n", strerror(errno));
    return 1;
  }
  char line[256];

  // Start copying baseFile...
  // cout << "Copying: nxt_base_file" << endl;
  while(fgets(line, sizeof line, baseFile)){
    // ... until token is found ...
    if(strstr(line, "//**INSERT NXC HERE**")){
      FILE *nxcFile = fopen(argv[1],"r");
      if(!nxcFile){
        fprintf(stderr, "Could not open: %s %s\n", argv[1], strerror(errno));
        return 1;
      }
      // ... then start copying NXC file ...
      // cout << "Token found. Copying: " << argv[1] << endl;
      while(fgets(line, sizeof line, nxcFile)){

        // ... change task main() for void *main2 ...
        if(strstr(line, "task main()") == line){
          char aux[256];
          strcpy(aux, line);
          strcpy(line,"void *main2(void* ptr)");
          memmove(aux, aux+11, strlen(aux)-10);
          strncat(line, aux, sizeof(aux));
        }

        // ... and sub for void ...
        if(strstr(line, "sub ") == line){
          char aux[256];
          strcpy(aux, line);
          strcpy(line,"void ");
          memmove(aux, aux+4, strlen(aux)-3);
          strncat(line, aux, sizeof(aux));
        }

        fputs(line, outputFile);
      }

      // ... until the end, and continue copying baseFile.
      // cout << "Finished: Copying: " << argv[1] << endl;
      // cout << "Resuming: Copying: nxt_base_file" << endl;
    }else{
      fputs(line, outputFile);
    }
  }

  cout << endl;
  cout << "File nxc_gazebo.cpp succesfully created." << endl;
  cout << "You can now compile your code. Just run the following command:" << endl;
  cout << "  rosmake nxt_gazebo_plugins" << endl << endl;

  return 0;
}

