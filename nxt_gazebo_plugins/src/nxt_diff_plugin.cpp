/*
 * Copyright (c) 2012, Miguel Gómez Gonzalo.
 * Copyright (c) 2012, Ana M. Cruz Martín.
 * Copyright (c) 2012, Juan A. Fernández Madrigal.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <algorithm>
#include <assert.h>

#include <nxt_gazebo_plugins/nxt_diff_plugin.h>

//copied from gazebo_ros_template
#include <gazebo/Global.hh>
#include <gazebo/XMLConfig.hh>
#include <gazebo/gazebo.h>
#include <gazebo/GazeboError.hh>
#include <gazebo/ControllerFactory.hh>

//added by myself
#include <gazebo/Simulator.hh>
#include <gazebo/Entity.hh>
#include <gazebo/Quatern.hh>
#include <gazebo/Controller.hh>
#include <gazebo/Body.hh>
#include <gazebo/World.hh>
#include <gazebo/PhysicsEngine.hh>

using namespace gazebo;

GZ_REGISTER_DYNAMIC_CONTROLLER("nxt_diff_plugin", NxtDiffPlugin);

enum
{
	RIGHT, LEFT
};

////////////////////////////////////////////////////////////////////////////////
// Constructor
NxtDiffPlugin::NxtDiffPlugin(Entity *parent)
    : Controller(parent)
{
    parent_ = dynamic_cast<Model*> (parent);
    
    if(!parent_)
      gzthrow("Nxt_Diff_Plugin controller requires a Model as its parent");
    
    enableMotors = true;
    
    wheelSpeed[RIGHT] = 0;
    wheelSpeed[LEFT] = 0;
    
    prevUpdateTime = Simulator::Instance()->GetSimTime();
    
    Param::Begin(&parameters);
    leftJointNameP = new ParamT<std::string> ("leftJoint", "", 1);
    rightJointNameP = new ParamT<std::string> ("rightJoint", "", 1);
    wheelSepP = new ParamT<float> ("wheelSeparation", 0.11, 1); //TODO: Change default if needed
    wheelDiamP = new ParamT<float> ("wheelDiameter", 0.056, 1); //TODO: Change default if needed
    torqueP = new ParamT<float> ("torque", 0.15, 1); //TODO: Change default if needed
    robotNamespaceP = new ParamT<std::string> ("robotNamespace", "/", 0);
    topicNameP = new ParamT<std::string> ("topicName", "", 1);
    Param::End();

}

////////////////////////////////////////////////////////////////////////////////
// Destructor
NxtDiffPlugin::~NxtDiffPlugin()
{
    delete leftJointNameP;
    delete rightJointNameP;
    delete wheelSepP;
    delete wheelDiamP;
    delete torqueP;
    delete robotNamespaceP;
    delete topicNameP;
    
}

////////////////////////////////////////////////////////////////////////////////
// Load the controller
void NxtDiffPlugin::LoadChild(XMLConfigNode *node)
{
    posIface_ = dynamic_cast<libgazebo::PositionIface*> (GetIface("position"));
    
    wheelSepP->Load(node);
    wheelDiamP->Load(node);
    torqueP->Load(node);
    leftJointNameP->Load(node);
    rightJointNameP->Load(node);
    
    joints[LEFT] = parent_->GetJoint(**leftJointNameP);
    joints[RIGHT] = parent_->GetJoint(**rightJointNameP);
    
    if(!joints[LEFT])
		gzthrow("The controller couldn't get left hinge joint");
	
    if(!joints[RIGHT])
		gzthrow("The controller couldn't get right hinge joint");
		
	robotNamespaceP->Load(node);
	
	robotNamespace = robotNamespaceP->GetValue();
	
	int argc = 0;
	char** argv = NULL;
	ros::init(argc, argv, "nxt_diff_plugin", ros::init_options::NoSigintHandler | ros::init_options::AnonymousName);
	rosnode_ = new ros::NodeHandle(robotNamespace);
	
	tf_prefix_ = tf::getPrefixParam(*rosnode_);
	transform_broadcaster_ = new tf::TransformBroadcaster();
	
	topicNameP->Load(node);
	topicName = topicNameP->GetValue();
	
	// ROS: Subscribe to the velocity command topic (usually "cmd_vel")
	sub_ = rosnode_->subscribe(topicName, 1, &NxtDiffPlugin::onCmdVel, this);
	pub_ = rosnode_->advertise<nav_msgs::Odometry>("odom", 1);
}

////////////////////////////////////////////////////////////////////////////////
// Initialize the controller
void NxtDiffPlugin::InitChild()
{
	odomPose[0] = 0.0;
	odomPose[1] = 0.0;
	odomPose[2] = 0.0;
	
	odomVel[0] = 0.0;
	odomVel[1] = 0.0;
	odomVel[2] = 0.0;
}

////////////////////////////////////////////////////////////////////////////////
// Update the controller
void NxtDiffPlugin::UpdateChild()
{
    /***************************************************************/
    /*                                                             */
    /*  this is called at every update simulation step             */
    /*                                                             */
    /***************************************************************/
    
    double wd, ws; // wheelDiameter and wheelSeparation
    double d1, d2; // distance traveled by each wheel
    double dr, da; // distance traveled by the robot and angle
    Time stepTime;
    
    wd = **(wheelDiamP);
    ws = **(wheelSepP);
    
    stepTime = Simulator::Instance()->GetSimTime() - prevUpdateTime;
    prevUpdateTime = Simulator::Instance()->GetSimTime();
    
    // Distance traveled by front wheels
    d1 = stepTime.Double() * wd / 2 * joints[LEFT]->GetVelocity(0);
    d2 = stepTime.Double() * wd / 2 * joints[RIGHT]->GetVelocity(0);
    
    // Distance traveled by the robot
    dr = (d1 + d2) / 2;
    da = (d1 - d2) / ws;
    
    // Compute odometric pose
    
    odomPose[0] += dr * cos(odomPose[2]);
    odomPose[1] += dr * sin(odomPose[2]);
    odomPose[2] += da;
    
    // Compute odometric instantaneous velocity
    odomVel[0] = dr / stepTime.Double();
    odomVel[1] = 0.0;
    odomVel[2] = da / stepTime.Double();
    
    if(enableMotors)
    {
		joints[LEFT]->SetVelocity(0, wheelSpeed[LEFT] / (**(wheelDiamP) / 2.0));
		joints[RIGHT]->SetVelocity(0, wheelSpeed[RIGHT] / (**(wheelDiamP) / 2.0));
		
		joints[LEFT]->SetMaxForce(0, **(torqueP));
		joints[RIGHT]->SetMaxForce(0, **(torqueP));
	}
}

////////////////////////////////////////////////////////////////////////////////
// Procedure for the subscriber
void NxtDiffPlugin::onCmdVel( const geometry_msgs::TwistConstPtr &msg)
{
	double vr, va;
	vr = msg->linear.x;
	va = msg->angular.z;
	
	wheelSpeed[LEFT] = vr - va * **(wheelSepP) / 2;
	wheelSpeed[RIGHT] = vr + va * **(wheelSepP) / 2;
}
