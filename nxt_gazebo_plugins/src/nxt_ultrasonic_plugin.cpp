/*
 * Copyright (c) 2012, Miguel Gómez Gonzalo.
 * Copyright (c) 2012, Ana M. Cruz Martín.
 * Copyright (c) 2012, Juan A. Fernández Madrigal.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <nxt_gazebo_plugins/nxt_ultrasonic_plugin.h>

#include <gazebo/Sensor.hh>
#include <gazebo/Global.hh>
#include <gazebo/XMLConfig.hh>
#include <gazebo/gazebo.h>
#include <gazebo/GazeboError.hh>
#include <gazebo/ControllerFactory.hh>
#include <gazebo/Simulator.hh>

using namespace gazebo;

GZ_REGISTER_DYNAMIC_CONTROLLER("nxt_ultrasonic_plugin", NxtUltrasonicPlugin);

////////////////////////////////////////////////////////////////////////////////
// Constructor
NxtUltrasonicPlugin::NxtUltrasonicPlugin(Entity *parent) : Controller(parent)
{
    parent_ = dynamic_cast<RaySensor*> (parent);
    
    if(!parent_)
      gzthrow("Nxt_Ultrasonic_Plugin controller requires a RaySensor as its parent");
    
    Param::Begin(&parameters);
    topicNameP_ = new ParamT<std::string>("topicName", "ultrasonic", false);
    frameNameP_ = new ParamT<std::string>("frameId", "", false);
    radiationP_ = new ParamT<std::string>("radiation", "ultrasound", false);
    fovP_ = new ParamT<double>("fov", 0.05, false);
    minRangeP_ = new ParamT<double>("minRange", 0.15, false);
    maxRangeP_ = new ParamT<double>("maxRange", 1.80, false);
    gaussianNoiseP_ = new ParamT<double>("gaussianNoise", 0.0, false);
    robotNamespaceP_ = new ParamT<std::string> ("robotNamespace", "", false);
    Param::End();
}

////////////////////////////////////////////////////////////////////////////////
// Destructor
NxtUltrasonicPlugin::~NxtUltrasonicPlugin()
{
    delete topicNameP_;
    delete frameNameP_;
    delete radiationP_;
    delete fovP_;
    delete minRangeP_;
    delete maxRangeP_;
    delete gaussianNoiseP_;
    delete robotNamespaceP_;
}

////////////////////////////////////////////////////////////////////////////////
// Load the controller
void NxtUltrasonicPlugin::LoadChild(XMLConfigNode *node)
{
    ROS_INFO("INFO: nxt_ultrasonic_plugin loading");

    topicNameP_->Load(node);
    frameNameP_->Load(node);
    radiationP_->Load(node);
    fovP_->Load(node);
    minRangeP_->Load(node);
    maxRangeP_->Load(node);
    gaussianNoiseP_->Load(node);
    robotNamespaceP_->Load(node);

    topicName_ = topicNameP_->GetValue();
    frameName_ = topicNameP_->GetValue();
    radiation_ = radiationP_->GetValue();
    fov_ = fovP_->GetValue();
    minRange_ = minRangeP_->GetValue();
    maxRange_ = maxRangeP_->GetValue();
    gaussianNoise_ = gaussianNoiseP_->GetValue();
    robotNamespace_ = robotNamespaceP_->GetValue();
}

////////////////////////////////////////////////////////////////////////////////
// Initialize the controller
void NxtUltrasonicPlugin::InitChild()
{
    Range.header.frame_id = **frameNameP_;

    if (radiation_ == std::string("ultrasound"))
        Range.radiation_type = sensor_msgs::Range::ULTRASOUND;
    else
        Range.radiation_type = sensor_msgs::Range::INFRARED;

    Range.field_of_view = fov_;
    Range.max_range = parent_->GetMaxRange();
    Range.min_range = parent_->GetMinRange();

    parent_->SetActive(false);
    rosnode_ = new ros::NodeHandle(robotNamespace_);
    pub_ = rosnode_->advertise<sensor_msgs::Range>(topicName_, 10);

}

////////////////////////////////////////////////////////////////////////////////
// Update the controller
void NxtUltrasonicPlugin::UpdateChild()
{
    /***************************************************************/
    /*                                                             */
    /*  this is called at every update simulation step             */
    /*                                                             */
    /***************************************************************/
    
    if(!parent_->IsActive()) parent_->SetActive(true);

    Range.header.stamp.sec = (Simulator::Instance()->GetSimTime()).sec;
    Range.header.stamp.nsec = (Simulator::Instance()->GetSimTime()).nsec;
    Range.range = std::numeric_limits<sensor_msgs::Range::_range_type>::max();

    for(int i = 0; i < parent_->GetRangeCount(); ++i){
        double ray = parent_->GetRange(i);
        if (ray < Range.range) Range.range = ray;
    }

    Range.range = Range.range + Range.min_range;

    Range.range = std::min(Range.range + this->GaussianKernel(0, gaussianNoise_), parent_->GetMaxRange());

    if(Range.range >= Range.max_range){
        Range.range = 2.555;
    }

    pub_.publish(Range);
    ros::spinOnce();

}

////////////////////////////////////////////////////////////////////////////////
// Finalize the controller
void NxtUltrasonicPlugin::FiniChild()
{
  parent_->SetActive(false);
  rosnode_->shutdown();
  delete rosnode_;
}

////////////////////////////////////////////////////////////////////////////////
// Utility for adding noise
double NxtUltrasonicPlugin::GaussianKernel(double mu,double sigma)
{
    // using Box-Muller transform to generate two independent standard normally disbributed normal variables
    // see wikipedia
    double U = (double)rand()/(double)RAND_MAX; // normalized uniform random variable
    double V = (double)rand()/(double)RAND_MAX; // normalized uniform random variable
    double X = sqrt(-2.0 * ::log(U)) * cos( 2.0*M_PI * V);
    //double Y = sqrt(-2.0 * ::log(U)) * sin( 2.0*M_PI * V); // the other indep. normal variable
    // we'll just use X
    // scale to our mu and sigma
    X = sigma * X + mu;
    return X;
}
