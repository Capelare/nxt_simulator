/*
 * Copyright (c) 2012, Miguel Gómez Gonzalo.
 * Copyright (c) 2012, Ana M. Cruz Martín.
 * Copyright (c) 2012, Juan A. Fernández Madrigal.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <nxt_gazebo_plugins/nxt_base_file.h>
#include <ros/ros.h>
#include <std_msgs/String.h>

#include <sstream>

using namespace std;

//** DO NOT EDIT ANYTHING ABOVE HERE **
#define LINE 2056
#define CORNER 197

void *main2(void* ptr){
  TextOut(1*6, LCD_LINE2, "ODOM:     ");

  for(int i=1;i<=4;i++){
    OnFwd(OUT_BC, 50);
    while(MotorRotationCount(OUT_B) < LINE);
    TextOut(7*6, LCD_LINE2, "    ");
    NumOut(7*6, LCD_LINE2, MotorRotationCount(OUT_B));
    OffEx(OUT_BC, RESET_ROTATION_COUNT);
    Wait(300);

    OnFwd(OUT_B, 25);
    OnRev(OUT_C, 25);
    while(MotorRotationCount(OUT_B) < CORNER);
    TextOut(7*6, LCD_LINE2, "    ");
    NumOut(7*6, LCD_LINE2, MotorRotationCount(OUT_B));
    OffEx(OUT_BC, RESET_ROTATION_COUNT);
    Wait(300);
  }
}
//**END OF NXC INSERTED**
//** DO NOT EDIT ANYTHING BELOW HERE **

int main(int argc, char **argv){

    ros::init(argc, argv, "nxc_gazebo");
    rosnode = new ros::NodeHandle("/");

    for(int i=0;i<4;i++) sensorsList_[i]=0;
    for(int i=0;i<7;i++) buttonPressCount_[i]=0;

    // launch the buttons handler as a new child
    pthread_create(&buttonsDaemon_thread, NULL, buttonsDaemon, NULL);

    ClearScreen(); // Start the LCD screen

    motors_pub = rosnode->advertise<nxt_gazebo_plugins::nxtMotors>("cmd_vel", 1);

    luminosity_sub = rosnode->subscribe("light_sensor", 1, &onLightMsg_);
    ultrasonic_sub = rosnode->subscribe("ultrasonic_sensor", 1, &onUltrasonicMsg_);
    odometry_sub = rosnode->subscribe("odom", 1, &onOdomMsg_);

    Wait(1000);

    //launch the nxc code as a new child
    pthread_create(&main2_thread, NULL, main2, NULL);

    struct sigaction sa;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = ctrlc_handler;
    // Establish a handler to catch CTRL+C and use it for exiting.
    if (sigaction(SIGINT, &sa, NULL) == -1) {
        perror("sigaction for Thread Termination failed");
        exit( EXIT_FAILURE );
    }

    // handle the callbacks for the subscribers
    while(ros::ok()){
        ros::spinOnce();
    }

    //kill the child with the nxc code
    pthread_cancel(main2_thread);
    pthread_cancel(buttonsDaemon_thread);

    rosnode->shutdown();
    delete rosnode;
    
    return 0;
}


// OTHER STUFF

void ctrlc_handler(int s, siginfo_t *siginf, void *ptr){
    resetTermios_();
    pthread_cancel(main2_thread);
    pthread_cancel(buttonsDaemon_thread);

    rosnode->shutdown();
    delete rosnode;
    
    exit(1);
}

// GENERIC NXC STUFF

void Wait(unsigned long ms){
    ros::Duration(ms/1000.0).sleep();
}

int Random(unsigned int n){
    return (rand() % n);
}

int Random(){
    return Random(32768)*(1-(2*Random(2))); //-32767..32767
}


// DISPLAY STUFF

void TextOut(int x, int y, string str){
    int n = (int)(x/6);
    if(n>=lineLength_) return;   //You're trying to write outside the LCD

    string auxStr = "";
    if(n>0) auxStr.assign(lcdLines_[y].c_str(), n);   //copy the first n characters
    auxStr += str;   //add what's in str
    n += str.length();
    lcdLines_[y].erase(0, n);    //remove the first n characters
    auxStr += lcdLines_[y];  //concatenate the rest of the characters
    auxStr = auxStr.erase(lineLength_);  //truncate the string
    //auxStr.erase(lineLength_, lcdLines_[y].length() - auxStr.length());
    
    lcdLines_[y] = auxStr;
    updateLCD_();

}

void NumOut(int x, int y, double value){
    // Simply convert value to str and call TextOut
    ostringstream auxStream;
    auxStream << value;

    TextOut(x, y, auxStream.str());
}

void ClearScreen(){
    ostringstream auxStream;
    for(int i=0;i<lineLength_;i++)
        auxStream << " ";
    
    for(int i=0;i<8;i++)
        lcdLines_[i] = auxStream.str(); //fill line with blank spaces
    

    updateLCD_();
}

void updateLCD_(){
    std::system("clear");

    cout << "+";
    for(int i=0;i<lineLength_;i++)
        cout << "-";
    cout << "+" << std::endl;

    for(int i=0;i<8;i++){
        cout << "|" << lcdLines_[i] << "|" << std::endl;
    }

    cout << "+";
    for(int i=0;i<lineLength_;i++)
        cout << "-";
    cout << "+" << std::endl;
    cout << std::endl;
    cout << "   <4] [5] [6>" << std::endl;
    cout << "       [2]" << std::endl;
    Wait(2);
}


// BUTTONS STUFF

void initTermios_(){
    tcgetattr(0, &oldterm_); // grab old terminal i/o settings
    newterm_ = oldterm_;  // make new settings same as old settings
    newterm_.c_lflag &= ~ICANON; // disable buffered i/o
    newterm_.c_lflag &= ~ECHO; // disable echo
    tcsetattr(0, TCSANOW, &newterm_);
}

void resetTermios_(){
    tcsetattr(0, TCSANOW, &oldterm_);
}

char getch_(){
    char ch;
    initTermios_();
    ch = getchar();
    resetTermios_();
    return ch;
}

void *buttonsDaemon(void* ptr){
    while(true){
        char c = getch_();
        int ci = c - '0'; // Convert to integer
        switch(ci){
            case BTNLEFT:
                buttonPressCount_[BTNLEFT]++;
                break;
            case BTNCENTER:
                buttonPressCount_[BTNCENTER]++;
                break;
            case BTNRIGHT:
                buttonPressCount_[BTNRIGHT]++;
                break;
            case BTNEXIT:
                buttonPressCount_[BTNEXIT]++;
                break;
            default:
                printf("%c\n", c);
                break; // ignore
        }
    }
}

int ButtonCount(const int btn, bool resetCount){
    int n = buttonPressCount_[btn];
    if(resetCount)
        buttonPressCount_[btn] = 0;

    return n;
}

int ButtonPressCount(const int btn){
    return ButtonCount(btn, false);
}

void SetButtonPressCount(const int btn, const int n){
    buttonPressCount_[btn] = n;
}



// OUTPUT STUFF (MOTORS)

void OnFwd(int outputs, int pwr){
    nxt_gazebo_plugins::nxtMotors msg;
    //OUT_B and OUT_C are RIGHT and LEFT motors respectively

    if(pwr > 100) pwr = 100;
    if(pwr < (-100)) pwr = -100;
    
    switch(outputs){
        case OUT_B:
            pwr_B = pwr/SPEED_REDUCTION_CONST;
            break;
        case OUT_C:
            pwr_C = pwr/SPEED_REDUCTION_CONST;
            break;
        case OUT_BC:
            pwr_C = pwr/SPEED_REDUCTION_CONST;
            pwr_B = pwr/SPEED_REDUCTION_CONST;
            break;
        default:
            break;
    }

    msg.left = pwr_C;
    msg.right = pwr_B;
    motors_pub.publish(msg);
    ros::spinOnce();
}

void OnRev(int outputs, int pwr){
    OnFwd(outputs, -(pwr));
}

void Off(int outputs){
    OnFwd(outputs, 0);
}

// functions involving odom
void ResetRotationCount (int outputs){
    switch(outputs){
        case OUT_B:
            odomZ_B = odom_B;
            break;
        case OUT_C:
            odomZ_C = odom_C;
            break;
        case OUT_BC:
            odomZ_B = odom_B;
            odomZ_C = odom_C;
            break;
        default:
            break;
    }
}

void OffEx (int outputs, const int reset){
    Off(outputs);
    if(reset==RESET_ROTATION_COUNT || reset==RESET_ALL)
        ResetRotationCount(outputs);
}

void OnFwdEx (int outputs, int pwr, const int reset){
    OnFwd(outputs, pwr);
    if(reset==RESET_ROTATION_COUNT || reset==RESET_ALL)
        ResetRotationCount(outputs);

}

void OnRevEx (int outputs, int pwr, const int reset){
    OnRev(outputs, pwr);
    if(reset==RESET_ROTATION_COUNT || reset==RESET_ALL)
        ResetRotationCount(outputs);

}

long MotorRotationCount(int outputs){
    switch(outputs){
        case OUT_B:
            return odom_B - odomZ_B;
            break;
        case OUT_C:
            return odom_C - odomZ_C;
            break;
        case OUT_BC:
        case OUT_AC:
        case OUT_ABC:
            ROS_ERROR("MotorRotationCount cannot handle multiple motors at once\n");
            return -1;
            break;
        default:
            ROS_ERROR("MotorRotationCount unknown parameter\n");
            break;
    }
}

void onOdomMsg_(const nxt_gazebo_plugins::nxtOdomConstPtr &msg){
    odom_C = (int) (msg->position.left * 180/M_PI);
    odom_B = (int) (msg->position.right * 180/M_PI);
    ros::spinOnce();
}


// INPUT STUFF (COMMON TO SENSORS)

void SetSensorLight(int port){
    sensorsList_[port] = IN_TYPE_LIGHT_INACTIVE;
}

void SetSensorLight(int port, bool bActive){
    //There's no difference between active and inactive at the moment
    SetSensorLight(port);
}

void SetSensorLowspeed(int port){
    sensorsList_[port] = IN_TYPE_LOWSPEED;
}

int Sensor(int port){
    int r = -1;
    // This switch block is intended for future use, when more sensors are supported
    switch(sensorsList_[port]){
        case IN_TYPE_NO_SENSOR:
            ROS_INFO("There's no sensor defined in S%d", port+1);
            break;
        case IN_TYPE_LIGHT_INACTIVE:
            // ROS_INFO("Sensor(S%d) = %d", port+1, lightSensorValue_);
            r = lightSensorValue_;
            break;
        default:
            ROS_INFO("There's an unknown sensor defined in S%d", port+1);
            break;
    }

    return r;
}

int SensorUS(int port){
    int r = -1;
    // This switch block is intended for future use, when more sensors are supported
    switch(sensorsList_[port]){
        case IN_TYPE_NO_SENSOR:
            ROS_INFO("There's no sensor defined in S%d", port+1);
            break;
        case IN_TYPE_LOWSPEED:
            //ROS_INFO("Sensor(S%d) = %d", port+1, ultrasonicSensorValue_);
            r = ultrasonicSensorValue_;
            break;
        default:
            ROS_INFO("There's an unknown sensor defined in S%d", port+1);
            break;
    }

    return r;
}

void onLightMsg_(const std_msgs::Int16 &msg){
    lightSensorValue_ = (int) (msg.data/2.55);
    ros::spinOnce();
    //ROS_INFO("onLightMsg_ = %d", lightSensorValue_);
}

void onUltrasonicMsg_(const sensor_msgs::Range &msg){
    float data = msg.range;

    ultrasonicSensorValue_ = (int) (data*100);
    ros::spinOnce();
    //ROS_INFO("ultrasonicSensor = %d", ultrasonicSensorValue_);
}
