/*
 * Copyright (c) 2012, Miguel Gómez Gonzalo.
 * Copyright (c) 2012, Ana M. Cruz Martín.
 * Copyright (c) 2012, Juan A. Fernández Madrigal.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef NXT_DIFF_PLUGIN_H
#define NXT_DIFF_PLUGIN_H

//copied from gazebo_ros_template
#include <gazebo/Controller.hh>
#include <gazebo/Body.hh>
#include <gazebo/World.hh>

//added by myself
#include <gazebo/Model.hh>
#include <gazebo/Param.hh>
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>

namespace gazebo
{
/// @addtogroup nxt_gazebo_plugins NXT Gazebo Plugins
/// @{
/** \defgroup NxtDiffPlugin

  \brief ROS interface to a Position2d controller for a Differential drive.
  
  TODO

  Example Usage:
  \verbatim
	TODO
  \endverbatim
 
\{
*/

class Joint;
class Entity;

class NxtDiffPlugin : public Controller
{
  /// \brief Constructor
  /// \param parent The parent entity, must be a Model or a Sensor
  public: NxtDiffPlugin(Entity *parent);

  /// \brief Destructor
  public: virtual ~NxtDiffPlugin();

  /// \brief Load the controller
  /// \param node XML config node
  protected: virtual void LoadChild(XMLConfigNode *node);

  /// \brief Init the controller
  protected: virtual void InitChild();

  /// \brief Update the controller
  protected: virtual void UpdateChild();

  private:
	void onCmdVel( const geometry_msgs::TwistConstPtr &msg);
	
    Model *parent_;
    
    Time prevUpdateTime;
    
    //Params
    ParamT<std::string> *leftJointNameP;
    ParamT<std::string> *rightJointNameP;
    ParamT<float> *wheelSepP;
    ParamT<float> *wheelDiamP;
    ParamT<float> *torqueP;
    ParamT<std::string> *robotNamespaceP;
    ParamT<std::string> *topicNameP;
    
    float wheelSpeed[2];
    bool enableMotors;
    
    libgazebo::PositionIface *posIface_;
    
    Joint *joints[2];
    
    std::string robotNamespace;
    std::string topicName;
    
    ros::NodeHandle* rosnode_;
    
    std::string tf_prefix_;
    tf::TransformBroadcaster *transform_broadcaster_;
    
    ros::Publisher pub_;
    ros::Subscriber sub_;
    
    float odomPose[3];
    float odomVel[3];
    
};

/** \} */
/// @}

}
#endif
