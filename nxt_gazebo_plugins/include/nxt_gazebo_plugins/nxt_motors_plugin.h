/*
 * Copyright (c) 2012, Miguel Gómez Gonzalo.
 * Copyright (c) 2012, Ana M. Cruz Martín.
 * Copyright (c) 2012, Juan A. Fernández Madrigal.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef NXT_MOTORS_PLUGIN_H
#define NXT_MOTORS_PLUGIN_H

//copied from gazebo_ros_template
#include <gazebo/Controller.hh>
#include <gazebo/Body.hh>
#include <gazebo/World.hh>

//added by myself
#include <gazebo/Model.hh>
#include <gazebo/Param.hh>
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
//#include <geometry_msgs/Twist.h>
#include <nxt_gazebo_plugins/nxtMotors.h>
#include <nxt_gazebo_plugins/nxtOdom.h>

namespace gazebo
{
/// @addtogroup nxt_gazebo_plugins NXT Gazebo Plugins
/// @{
/** \defgroup NxtMotorsPlugin

  \brief ROS interface to a Position2d controller for a dual motor drive.
  
  TODO

  Example Usage:
  \verbatim
	TODO
  \endverbatim
 
\{
*/

class Joint;
class Entity;

class NxtMotorsPlugin : public Controller
{
  /// \brief Constructor
  /// \param parent The parent entity, must be a Model or a Sensor
  public: NxtMotorsPlugin(Entity *parent);

  /// \brief Destructor
  public: virtual ~NxtMotorsPlugin();

  /// \brief Load the controller
  /// \param node XML config node
  protected: virtual void LoadChild(XMLConfigNode *node);

  /// \brief Init the controller
  protected: virtual void InitChild();

  /// \brief Update the controller
  protected: virtual void UpdateChild();

  /// \brief Receive speed messages
  private: void onCmdVel( const nxt_gazebo_plugins::nxtMotorsConstPtr &msg);

  /// \brief The parent model
  private: Model *parent_;

  /// \brief Pointer to ros node
  private: ros::NodeHandle* rosnode_;

  /// \brief Publisher and Subscriber
  private: ros::Publisher pub_;
  private: ros::Subscriber sub_;

  /// \brief Topic name for the subscriber
  private: ParamT<std::string> *subTopicNameP_;
  private: std::string subTopicName_;

  /// \brief Topic name for the publisher
  private: ParamT<std::string> *pubTopicNameP_;
  private: std::string pubTopicName_;

  /// \brief for setting ROS name space
  private: ParamT<std::string> *robotNamespaceP_;
  private: std::string robotNamespace_;

  /// \brief update Time
  private: Time prevUpdateTime_;

  /// \brief left joint name
  private: ParamT<std::string> *leftJointNameP_;
  private: std::string leftJointName_;

  /// \brief right joint name
  private: ParamT<std::string> *rightJointNameP_;
  private: std::string rightJointName_;

  /// \brief torque of the joints
  private: ParamT<float> *torqueP_;
  private: float torque_;

  /// \brief Speed of the wheels
  private: float wheelSpeed_[2];

  /// \brief motors enabled or not?
  private: bool enableMotors_;

  /// \brief Position interface
  private: libgazebo::PositionIface *posIface_;

  /// \brief Joints
  private: Joint *joints_[2];

  /// \brief Odometry msgs
  private: float odomPose_[2];
  private: float odomVel_[2];
  private: nxt_gazebo_plugins::nxtOdom Odom_;

    
};

/** \} */
/// @}

}
#endif
