/*
 * Copyright (c) 2012, Miguel Gómez Gonzalo.
 * Copyright (c) 2012, Ana M. Cruz Martín.
 * Copyright (c) 2012, Juan A. Fernández Madrigal.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef NXT_ULTRASONIC_PLUGIN_H
#define NXT_ULTRASONIC_PLUGIN_H

//copied from gazebo_ros_template
#include <gazebo/Controller.hh>
#include <gazebo/Entity.hh>
#include <gazebo/Model.hh>
#include <gazebo/RaySensor.hh>
#include <gazebo/Param.hh>


#define USE_CBQ
#ifdef USE_CBQ
#include <ros/callback_queue.h>
#include <ros/ros.h>
#endif

#include <sensor_msgs/Range.h>

namespace gazebo
{
/// @addtogroup nxt_gazebo_plugins NXT Gazebo Plugins
/// @{
/** \defgroup NxtUltrasonicPlugin

  \brief ROS interface to a Range controller for the NXT's Ultrasonic sensor.
  
  TODO

  Example Usage:
  \verbatim
	TODO
  \endverbatim
 
\{
*/

class UltrasonicSensor;

class NxtUltrasonicPlugin : public Controller
{
  /// \brief Constructor
  /// \param parent The parent entity, must be a Model or a Sensor
  public: NxtUltrasonicPlugin(Entity *parent);

  /// \brief Destructor
  public: virtual ~NxtUltrasonicPlugin();

  /// \brief Load the controller
  /// \param node XML config node
  protected: virtual void LoadChild(XMLConfigNode *node);

  /// \brief Init the controller
  protected: virtual void InitChild();

  /// \brief Update the controller
  protected: virtual void UpdateChild();

  /// \brief Fini the controller
  protected: virtual void FiniChild();

  /// \brief The parent sensor
  private: RaySensor *parent_;

  /// \brief pointer to ros node
  private: ros::NodeHandle* rosnode_;
  private: ros::Publisher pub_;

  /// \brief ros message
  private: sensor_msgs::Range Range;
 
  /// \brief topic name
  private: ParamT<std::string> *topicNameP_;
  private: std::string topicName_;

  /// \brief frame transform name, should match link name
  private: ParamT<std::string> *frameNameP_;
  private: std::string frameName_;

  /// \brief radiation type, should be ultrasound or infrared
  private: ParamT<std::string> *radiationP_;
  private: std::string radiation_;

  /// \brief field of view
  private: ParamT<double> *fovP_;
  private: double fov_;

  /// \brief minimum range
  private: ParamT<double> *minRangeP_;
  private: double minRange_;

  /// \brief maximum range
  private: ParamT<double> *maxRangeP_;
  private: double maxRange_;

  /// \brief Gaussian noise
  private: ParamT<double> *gaussianNoiseP_;
  private: double gaussianNoise_;

  /// \brief Gaussian noise generator
  private: double GaussianKernel(double mu,double sigma);

  /// \brief for setting ROS name space
  private: ParamT<std::string> *robotNamespaceP_;
  private: std::string robotNamespace_;

};

/** \} */
/// @}

}
#endif
