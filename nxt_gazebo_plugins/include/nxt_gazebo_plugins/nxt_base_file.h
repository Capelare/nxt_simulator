/*
 * Copyright (c) 2012, Miguel Gómez Gonzalo.
 * Copyright (c) 2012, Ana M. Cruz Martín.
 * Copyright (c) 2012, Juan A. Fernández Madrigal.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef NXT_BASE_FILE_H
#define NXT_BASE_FILE_H

#define M_PI 3.14159265

#include <ros/ros.h>
#include <cstdlib>
#include <pthread.h>
#include <termios.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <std_msgs/Int16.h>
#include <sensor_msgs/Range.h>
#include <nxt_gazebo_plugins/nxtOdom.h>
#include <nxt_gazebo_plugins/nxtMotors.h>


using namespace std;

// ROS STUFF
ros::NodeHandle* rosnode;

// ROS TOPICS
ros::Publisher motors_pub;

ros::Subscriber ultrasonic_sub;
ros::Subscriber luminosity_sub;
ros::Subscriber odometry_sub;


// OTHER STUFF
pthread_t main2_thread;
pthread_t buttonsDaemon_thread;

void ctrlc_handler(int s, siginfo_t *siginf, void *ptr);


// GENERIC NXC STUFF

void Wait(unsigned long ms);
int Random(unsigned int n);
int Random();


// DISPLAY STUFF

#define LCD_LINE1 0
#define LCD_LINE2 1
#define LCD_LINE3 2
#define LCD_LINE4 3
#define LCD_LINE5 4
#define LCD_LINE6 5
#define LCD_LINE7 6
#define LCD_LINE8 7

string lcdLines_[8];
int lineLength_ = 16;

void TextOut(int x, int y, string str);
void NumOut(int x, int y, double value);
void ClearScreen();
void updateLCD_();


// BUTTONS STUFF

#define BTN1 2
#define BTN2 6
#define BTN3 4
#define BTN4 5
#define BTNEXIT BTN1
#define BTNRIGHT BTN2
#define BTNLEFT BTN3
#define BTNCENTER BTN4

static struct termios oldterm_, newterm_;
int buttonPressCount_[7]; // Not optimal, but allows direct access with BTNx constants.

void initTermios_();
void resetTermios_();
char getch_();
void *buttonsDaemon(void* ptr);

int ButtonCount(const int btn, bool resetCount);
int ButtonPressCount(const int btn);
void SetButtonPressCount(const int btn, const int n);


// OUTPUT STUFF (MOTORS)

#define SPEED_REDUCTION_CONST 10.0

#define OUT_A   1
#define OUT_B   2
#define OUT_AB  3
#define OUT_C   4
#define OUT_AC  5
#define OUT_BC  6
#define OUT_ABC 7

#define RESET_NONE 0            // 0x00
#define RESET_COUNT 8           // 0x08
#define RESET_BLOCK_COUNT 32    // 0x20
#define RESET_ROTATION_COUNT 64 // 0x40
#define RESET_BLOCKANDTACHO 40  // 0x28
#define RESET_ALL 104           // 0x68

float pwr_B = 0;
float pwr_C = 0;

long odom_B = 0;
long odom_C = 0;

long odomZ_B = 0; // this is for resetting the odometry values
long odomZ_C = 0;

void OnFwd(int outputs, int pwr);
void OnRev(int outputs, int pwr);
void Off(int outputs);

// functions involving odom
void ResetRotationCount (int outputs);
void OffEx (int outputs, const int reset);
void OnFwdEx (int outputs, char pwr, const int reset);
void OnRevEx (int outputs, char pwr, const int reset);
long MotorRotationCount(int outputs);

void onOdomMsg_(const nxt_gazebo_plugins::nxtOdomConstPtr &msg);


// INPUT STUFF (COMMON TO SENSORS)

#define IN_1 0
#define IN_2 1
#define IN_3 2
#define IN_4 3

#define S1 0
#define S2 1
#define S3 2
#define S4 3

#define IN_TYPE_NO_SENSOR 0
#define IN_TYPE_LIGHT_INACTIVE 6
#define IN_TYPE_LOWSPEED 10

int sensorsList_[4];
int lightSensorValue_;
int ultrasonicSensorValue_;


void SetSensorLight(int port);
void SetSensorLight(int port, bool bActive);
void SetSensorLowspeed(int port);
int Sensor(int port);
int SensorUS(int port);

void onLightMsg_(const std_msgs::Int16 &msg);
void onUltrasonicMsg_(const sensor_msgs::Range &msg);


#endif