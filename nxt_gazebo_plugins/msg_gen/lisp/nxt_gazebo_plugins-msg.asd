
(cl:in-package :asdf)

(defsystem "nxt_gazebo_plugins-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "nxtMotors" :depends-on ("_package_nxtMotors"))
    (:file "_package_nxtMotors" :depends-on ("_package"))
    (:file "nxtOdom" :depends-on ("_package_nxtOdom"))
    (:file "_package_nxtOdom" :depends-on ("_package"))
  ))