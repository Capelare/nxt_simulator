; Auto-generated. Do not edit!


(cl:in-package nxt_gazebo_plugins-msg)


;//! \htmlinclude nxtOdom.msg.html

(cl:defclass <nxtOdom> (roslisp-msg-protocol:ros-message)
  ((position
    :reader position
    :initarg :position
    :type nxt_gazebo_plugins-msg:nxtMotors
    :initform (cl:make-instance 'nxt_gazebo_plugins-msg:nxtMotors))
   (speed
    :reader speed
    :initarg :speed
    :type nxt_gazebo_plugins-msg:nxtMotors
    :initform (cl:make-instance 'nxt_gazebo_plugins-msg:nxtMotors)))
)

(cl:defclass nxtOdom (<nxtOdom>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <nxtOdom>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'nxtOdom)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name nxt_gazebo_plugins-msg:<nxtOdom> is deprecated: use nxt_gazebo_plugins-msg:nxtOdom instead.")))

(cl:ensure-generic-function 'position-val :lambda-list '(m))
(cl:defmethod position-val ((m <nxtOdom>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader nxt_gazebo_plugins-msg:position-val is deprecated.  Use nxt_gazebo_plugins-msg:position instead.")
  (position m))

(cl:ensure-generic-function 'speed-val :lambda-list '(m))
(cl:defmethod speed-val ((m <nxtOdom>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader nxt_gazebo_plugins-msg:speed-val is deprecated.  Use nxt_gazebo_plugins-msg:speed instead.")
  (speed m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <nxtOdom>) ostream)
  "Serializes a message object of type '<nxtOdom>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'position) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'speed) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <nxtOdom>) istream)
  "Deserializes a message object of type '<nxtOdom>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'position) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'speed) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<nxtOdom>)))
  "Returns string type for a message object of type '<nxtOdom>"
  "nxt_gazebo_plugins/nxtOdom")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'nxtOdom)))
  "Returns string type for a message object of type 'nxtOdom"
  "nxt_gazebo_plugins/nxtOdom")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<nxtOdom>)))
  "Returns md5sum for a message object of type '<nxtOdom>"
  "60309b6a802dc973e40e49b8f45a0392")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'nxtOdom)))
  "Returns md5sum for a message object of type 'nxtOdom"
  "60309b6a802dc973e40e49b8f45a0392")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<nxtOdom>)))
  "Returns full string definition for message of type '<nxtOdom>"
  (cl:format cl:nil "nxtMotors position~%nxtMotors speed~%================================================================================~%MSG: nxt_gazebo_plugins/nxtMotors~%float64 left~%float64 right~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'nxtOdom)))
  "Returns full string definition for message of type 'nxtOdom"
  (cl:format cl:nil "nxtMotors position~%nxtMotors speed~%================================================================================~%MSG: nxt_gazebo_plugins/nxtMotors~%float64 left~%float64 right~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <nxtOdom>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'position))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'speed))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <nxtOdom>))
  "Converts a ROS message object to a list"
  (cl:list 'nxtOdom
    (cl:cons ':position (position msg))
    (cl:cons ':speed (speed msg))
))
